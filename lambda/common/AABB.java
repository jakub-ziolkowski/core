package lambda.common;

import java.awt.geom.Point2D;

/**
 * Bounding Box
 *
 * @author jakub.ziolkowski
 */
public class AABB {

 public Point2D.Float rightBottom, topLeft;

 public AABB() {
  rightBottom = new Point2D.Float();
  topLeft = new Point2D.Float();
 }

 @Override
 public String toString() {
  return "AABB{" + "lowerBound=" + rightBottom + ", upperBound=" + topLeft + '}';
 }

 public boolean intersection(AABB aabb) {
  return !(aabb.topLeft.x > this.rightBottom.x
           || aabb.rightBottom.x < this.topLeft.x
           || aabb.topLeft.y > this.rightBottom.y
           || aabb.rightBottom.y < this.topLeft.y);
 }

 public boolean contains(AABB aabb) {
  boolean result = true;
  result = result && this.rightBottom.x <= aabb.rightBottom.x;
  result = result && this.rightBottom.y <= aabb.rightBottom.y;
  result = result && aabb.topLeft.x <= this.topLeft.x;
  result = result && aabb.topLeft.y <= this.topLeft.y;
  return result;
 }
}