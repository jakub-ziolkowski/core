package lambda.common;

import java.awt.geom.Point2D;
import lambda.core.LConstraint;
import lambda.core.LParticle;

/**
 * Collision data holder, used for processCollision
 *
 * @see LBody.java
 * @author jakub.ziolkowski
 */
public class CollisionData {

 public static String otherClass;
 public static float depth;
 public static Point2D.Float normal = new Point2D.Float(0, 0);
 public static LConstraint e;
 public static LParticle v;
}
