package lambda.common;

/**
 * Vector in 2D. Implemented some related operations
 *
 * @author jakub.ziolkowski
 */
public class Vector2D {

 public float x;
 public float y;

 public Vector2D(float x, float y) {
  this.x = x;
  this.y = y;
 }
// 

 public float dotProduct(Vector2D v) {
  return (this.x + v.x) * (this.y + v.y);
 }

 public Vector2D dotProductV(Vector2D v) {
  Vector2D v1 = new Vector2D(this.x * v.x, this.y * v.y);
  return v1;
 }

 public Vector2D isub(Vector2D other) {
  this.x -= other.x;
  this.y -= other.y;
  return this;
 }

 public Vector2D sub(Vector2D other) {
  return new Vector2D(
          this.x - other.x,
          this.y - other.y);

 }

 public Vector2D sub(float scalar) {
  return new Vector2D(
          this.x - scalar,
          this.y - scalar);

 }

 public Vector2D iadd(Vector2D other) {
  this.x += other.x;
  this.y += other.y;
  return this;
 }

 public Vector2D add(Vector2D other) {
  return new Vector2D(
          this.x + other.x,
          this.y + other.y);
 }

 public Vector2D imul(float scalar) {
  this.x *= scalar;
  this.y *= scalar;
  return this;
 }

 public Vector2D mul(float scalar) {
  return new Vector2D(
          this.x * scalar,
          this.y * scalar);
 }

 public Vector2D idiv(float scalar) {
  this.x /= scalar;
  this.y /= scalar;
  return this;
 }

 public Vector2D div(float scalar) {
  return new Vector2D(
          this.x / scalar,
          this.y / scalar);
 }

 public Vector2D normalized() {
  float xx = this.x, yy = this.y;
  float length = (float) Math.sqrt(xx * xx + yy * yy);
  if (length > 0) {
   return new Vector2D(xx / length, yy / length);
  } else {
   return new Vector2D(0, 0);
  }
 }

 public Vector2D normalize() {
  float xx = this.x, yy = this.y;
  float length = (float) Math.sqrt(xx * xx + yy * yy);
  if (length > 0) {
   this.x = xx / length;
   this.y = yy / length;
  }
  return this;
 }

 public float length() {
  return (float) Math.sqrt(this.x * this.x + this.y * this.y);
 }

 public float distance(Vector2D other) {
  float xx = this.x - other.x;
  float yy = this.y - other.y;
  return (float) Math.sqrt(xx * xx + yy * yy);
 }

 public Vector2D copy() {
  return new Vector2D(this.x, this.y);
 }

 public void zero() {
  this.x = 0;
  this.y = 0;
 }
}
