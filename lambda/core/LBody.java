package lambda.core;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import lambda.common.AABB;

/**
 * Abstract representation of physics body in system All
 * 'collidable'/'renderable' objects must implement it.
 *
 * @author jakub.ziolkowski
 */
public abstract class LBody {

 LBody(LWorld w) {
  this.world = w;
 }
 public AABB aabb = new AABB();
 public Point2D.Float center = new Point2D.Float(0, 0);
 public float mass = 1;
 public LWorld world;

 public abstract void updateAABB();

 public abstract void processCollision();

 public abstract boolean detectCollision(LBody other);

 public abstract void simulate(float delta);

 public abstract void render(Graphics2D g);
}
