package lambda.core;

import java.awt.Color;
import java.awt.Graphics2D;
import lambda.common.Vector2D;

/**
 * Satisfy constant distance between particles in time
 * Used in Polygon creation
 *
 * @see LParticle
 * @see LPolygon
 * @author jakub.ziolkowski
 */
public class LConstraint {

 public LBody parent;
 public LParticle particle1, particle2, collisionPoint;
 public final float d;

 public LConstraint(LBody parent, LParticle point1, LParticle point2) {
  this.particle1 = point1;
  this.particle2 = point2;
  this.parent = parent;
  this.d = point1.position.distance(point2.position);

  if (parent instanceof LPolygon) {
   ((LPolygon) parent).addConstraint(this);
  }
 }

 public void solve() {
  LConstraint e = this;
  float v1v2x = e.particle2.position.x - e.particle1.position.x;
  float v1v2y = e.particle2.position.y - e.particle1.position.y;

  float v1v2Length = (float) Math.hypot(v1v2x, v1v2y);
  float diff = v1v2Length - e.d;
  float len = 1.0f / (float) Math.hypot(v1v2x, v1v2y);
  v1v2x *= len;
  v1v2y *= len;
  e.particle1.position.x += v1v2x * diff * 0.5f;
  e.particle1.position.y += v1v2y * diff * 0.5f;
  e.particle2.position.x -= v1v2x * diff * 0.5f;
  e.particle2.position.y -= v1v2y * diff * 0.5f;
 }

 public void draw(Graphics2D g) {
  Vector2D pos1 = this.particle1.position;
  Vector2D pos2 = this.particle2.position;
  g.setColor(Color.BLUE);
  if (collisionPoint != null) {
   g.fillOval((int) (collisionPoint.position.x), (int) (collisionPoint.position.y), (int) 5, (int) 5);
  }

  g.drawLine((int) pos1.x, (int) pos1.y, (int) pos2.x, (int) pos2.y);

 }
}
