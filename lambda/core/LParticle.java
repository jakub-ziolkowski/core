package lambda.core;

import lambda.common.Vector2D;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 * Simple verlet intergation particle processing Used in Polygon creation
 *
 * @see LPolygon
 * @see LConstraint
 * @author jakub.ziolkowski
 */
public class LParticle {

 public LBody parent;
 public Vector2D position, oldPosition, acceleration;

 public LParticle(LBody parent, float x, float y) {
  this.parent = parent;
  this.position = new Vector2D(x, y);
  this.oldPosition = new Vector2D(x, y);
  this.acceleration = new Vector2D(0, 0);
 }

 public void accelerate(Vector2D v) {
  this.acceleration.iadd(v);
 }

 public void correct(Vector2D v) {
  this.position.iadd(v);
 }

 public void simulate(float delta) {
  this.acceleration.imul(delta * delta);

  Vector2D pos = this.position
          .mul(2.0f)
          .sub(this.oldPosition)
          .add(this.acceleration);
  this.oldPosition = this.position;
  this.position = pos;
 }

 public void draw(Graphics2D g) {
  g.setBackground(Color.blue);
  g.fillOval((int) (position.x), (int) (position.y), 5, 5);
 }
}
