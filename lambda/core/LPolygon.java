package lambda.core;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import lambda.common.AABB;
import lambda.common.CollisionData;
import lambda.common.MinMax;
import lambda.common.Vector2D;

/**
 * Polygon body object
 *
 * @author jakub.ziolkowski
 */
public class LPolygon extends LBody {

 public ArrayList<LParticle> particles = new ArrayList<LParticle>();
 public ArrayList<LConstraint> constraints = new ArrayList<LConstraint>();

 public LPolygon(LWorld w) {
  super(w);
 }

 public void addConstraint(LConstraint lc) {
  this.addParticle(lc.particle1);
  this.addParticle(lc.particle2);
  this.constraints.add(lc);
 }

 public void addParticle(LParticle lp) {
  if (!this.particles.contains(lp)) {
   this.particles.add(lp);
  }
 }

 @Override
 public void updateAABB() {
  float minx = particles.get(0).position.x, maxx = particles.get(0).position.x,
          miny = particles.get(0).position.y, maxy = particles.get(0).position.y;
  for (LParticle lPoint : particles) {
   if (minx > lPoint.position.x) {
    minx = lPoint.position.x;
   }
   if (miny > lPoint.position.y) {
    miny = lPoint.position.y;
   }
   if (maxx < lPoint.position.x) {
    maxx = lPoint.position.x;
   }
   if (maxy < lPoint.position.y) {
    maxy = lPoint.position.y;
   }
  }
  aabb.topLeft.x = minx;
  aabb.topLeft.y = miny;
  aabb.rightBottom.x = maxx;
  aabb.rightBottom.y = maxy;
 }
 private float minX, maxX, minY, maxY;

 public void calculateCenter() {
  int size = particles.size();
  center.x = 0;
  center.y = 0;

  minX = 10000.0f;
  minY = 10000.0f;
  maxX = -10000.0f;
  maxY = -10000.0f;

  for (int i = 0; i < size; i++) {
   center.x += particles.get(i).position.x;
   center.y += particles.get(i).position.y;

   minX = Math.min(minX, particles.get(i).position.x);
   minY = Math.min(minY, particles.get(i).position.y);
   maxX = Math.max(maxX, particles.get(i).position.x);
   maxY = Math.max(maxX, particles.get(i).position.y);
  }

  center.x /= size;
  center.y /= size;
 }

 public MinMax ProjectToAxis(Point2D.Float Axis) {
  float dotP = Axis.x * particles.get(0).position.x + Axis.y * particles.get(0).position.y;
  MinMax data = new MinMax();
  data.min = dotP;
  data.max = dotP;
  for (int i = 0; i < particles.size(); i++) {
   dotP = Axis.x * particles.get(i).position.x + Axis.y * particles.get(i).position.y;
   data.min = Math.min(dotP, data.min);
   data.max = Math.max(dotP, data.max);
  }
  return data;
 }

 @Override
 public void processCollision() {
  if (CollisionData.otherClass.contains("LPolygon")) {

   LParticle p1 = CollisionData.e.particle1;
   LParticle p2 = CollisionData.e.particle2;
   float collisionVectorX = CollisionData.normal.x * CollisionData.depth;
   float collisionVectorY = CollisionData.normal.y * CollisionData.depth;

   float t;
   if (Math.abs(p1.position.x - p2.position.x) > Math.abs(p1.position.y - p2.position.y)) {
    t = (CollisionData.v.position.x - collisionVectorX - p1.position.x) / (p2.position.x - p1.position.x);
   } else {
    t = (CollisionData.v.position.y - collisionVectorY - p1.position.y) / (p2.position.y - p1.position.y);
   }

   float lambda = 1.0f / (t * t + (1 - t) * (1 - t));

   float edgeMass = t * p2.parent.mass + (1f - t) * p1.parent.mass;
   float invCollisionMass = 1.0f / (edgeMass + CollisionData.v.parent.mass);

   float ratio1 = CollisionData.v.parent.mass * invCollisionMass;
   float ratio2 = edgeMass * invCollisionMass;

   p1.position.x -= collisionVectorX * ((1 - t) * ratio1 * lambda);
   p1.position.y -= collisionVectorY * ((1 - t) * ratio1 * lambda);

   p2.position.x -= collisionVectorX * (t * ratio1 * lambda);
   p2.position.y -= collisionVectorY * (t * ratio1 * lambda);

   CollisionData.v.position.x += collisionVectorX * ratio2;
   CollisionData.v.position.y += collisionVectorY * ratio2;
   
   CollisionData.v.oldPosition.x += (CollisionData.v.position.x - CollisionData.v.oldPosition.x) * ratio2;
   CollisionData.v.oldPosition.y += (CollisionData.v.position.y - CollisionData.v.oldPosition.y) * ratio2;


  }
 }
 private static Point2D.Float axis = new Point2D.Float(0, 0);

 private static float id(float minA, float maxA, float minB, float maxB) {
  if (minA < minB) {
   return minB - maxA;
  } else {
   return minA - maxB;
  }
 }

 @Override
 public boolean detectCollision(LBody other) {
  if (other instanceof LPolygon) {
   float md = 10000.0f;
   LBody b1;
   for (int i = 0; i < this.constraints.size() + ((LPolygon) other).constraints.size(); i++) {
    LConstraint lc;

    if (i < this.constraints.size()) {
     lc = this.constraints.get(i);
    } else {
     lc = this.constraints.get(i - ((LPolygon) other).constraints.size());
    }

    axis.x = lc.particle1.position.y - lc.particle2.position.y;
    axis.y = lc.particle2.position.x - lc.particle1.position.x;

    float len = 1.0f / (float) Math.hypot(axis.x, axis.y);
    axis.x *= len;
    axis.y *= len;

    MinMax dataA = this.ProjectToAxis(axis);
    MinMax dataB = ((LPolygon) other).ProjectToAxis(axis);

    float d = id(dataA.min, dataA.max, dataB.min, dataB.max);
    if (d > 0.0f) {
     return false;
    } else if (Math.abs(d) < md) {
     md = Math.abs(d);

     CollisionData.normal.x = axis.x;
     CollisionData.normal.y = axis.y;
     CollisionData.e = lc;
    }
   }

   CollisionData.depth = md;
   
   b1 = this;

   if (CollisionData.e != null) {
    if (CollisionData.e.parent != other) {
     LPolygon tmp = (LPolygon) other;
     other = this;
     b1 = tmp;
    }
   }

   float xx = b1.center.x - other.center.x;
   float yy = b1.center.y - other.center.y;
   float m = CollisionData.normal.x * xx + CollisionData.normal.y * yy;

   if (m < 0) {
    CollisionData.normal.x = 0 - CollisionData.normal.x;
    CollisionData.normal.y = 0 - CollisionData.normal.y;
   }

   float smallestD = 10000.0f;
   for (int i = 0; i < ((LPolygon)b1).particles.size(); i++) {
    xx = ((LPolygon)b1).particles.get(i).position.x - other.center.x;
    yy = ((LPolygon)b1).particles.get(i).position.y - other.center.y;
    float d = CollisionData.normal.x * xx + CollisionData.normal.y * yy;

    if (d < smallestD) {
     smallestD = d;
     CollisionData.v = ((LPolygon)b1).particles.get(i);
    }
   }

   CollisionData.otherClass = other.getClass().getName();
   return true;
  }
  return false;
 }

 public void processBorderCollision() {
  for (LParticle lp : this.particles) {
   this.processBorderCollision(lp);
  }
 }

 public void processBorderCollision(LParticle p) {
  float x = p.position.x;
  float y = p.position.y;
 // System.out.println("pbp");
  float width = world.canvas.width;
  float height = world.canvas.height-100;
  float damping = 0.01f;
  float radius = 5;

  if (x - radius < 0) {
   float vx = (p.oldPosition.x - p.position.x) * damping;
   p.position.x = radius;
   p.oldPosition.x = p.position.x - vx;
  } else if (x + radius > width - 5) {
   float vx = (p.oldPosition.x - p.position.x) * damping;
   p.position.x = width - radius;
   p.oldPosition.x = p.position.x - vx;
  }
  if (y - radius < 0) {
   float vy = (p.oldPosition.y - p.position.y) * damping;
   p.position.y = radius;
   p.oldPosition.y = p.position.y - vy;
  } else if (y + radius > height - 5) {
   float vy = (p.oldPosition.y - p.position.y) * damping;
   p.position.y = height - radius;
   p.oldPosition.y = p.position.y - vy;
  }
 }

 @Override
 public void simulate(float steps) {
  float delta = (float) (1.0 / steps);

  for (int j = 0; j < steps; j++) {
   this.updateAABB();

   this.processBorderCollision();
   for (LBody wb : world.bodies) {
    if (this != wb) {
     if (this.aabb.intersection(wb.aabb)) {
      if (this.detectCollision(wb)) {
      // System.out.println("process colision");
       this.processCollision();
      }
     }
    }
   }

   for (LConstraint lc : constraints) {
    lc.solve();
   }
   
   this.calculateCenter();
   for (LParticle lp : particles) {
    lp.accelerate(new Vector2D(0, world.gravity));
    lp.simulate(delta);
   }

  }
 }

 @Override
 public void render(Graphics2D g) {
  g.setColor(Color.red);
  g.setBackground(Color.red);

  Polygon p = new Polygon();
  for (LParticle lp : particles) {
   p.addPoint((int) lp.position.x, (int) lp.position.y);
   g.fillRect((int) lp.position.x, (int) lp.position.y, 5, 5);
  }
  g.setColor(Color.red);
  g.drawPolygon(p);
  //System.out.println("prrr");
 }
}
