package lambda.core;

import java.awt.Graphics2D;
import java.util.ArrayList;
import lambda.ui.GameCanvas;

/**
 * Physics sandbox with objects on 'map' boundary TODO: Camera, Map loading etc.
 *
 * @author jakub.ziolkowski
 */
public class LWorld {

 protected GameCanvas canvas;
 public float gravity = 0.5f;
 public float physicsSteps = 6;
 public ArrayList<LBody> bodies = new ArrayList<LBody>();

 public LWorld(GameCanvas gc) {
  this.canvas = gc;
 }

 public void addBody(LBody b) {
  if (!this.bodies.contains(b)) {
   this.bodies.add(b);
  }
 }

 public void simulate(Graphics2D bkG) {
  bkG.clearRect(0, 0, this.canvas.width, this.canvas.height);
  for (LBody lb : bodies) {
   lb.simulate(physicsSteps);
   lb.render(bkG);
  }
 }

 public void createBox(int x, int y, int width, int height, int mass) {

  LBody b = new LPolygon(this);
  LParticle V1 = new LParticle(b, x, y);
  LParticle V2 = new LParticle(b, x + width, y);
  LParticle V3 = new LParticle(b, x + width, y + height);
  LParticle V4 = new LParticle(b, x, y + height);

  new LConstraint(b, V1, V2);
  new LConstraint(b, V2, V3);
  new LConstraint(b, V3, V4);
  new LConstraint(b, V4, V1);
  new LConstraint(b, V1, V3);
  new LConstraint(b, V2, V4);


  b.mass = mass;

  this.addBody(b);
 }
}
