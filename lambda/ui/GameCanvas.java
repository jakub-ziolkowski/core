package lambda.ui;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.Timer;
import java.util.TimerTask;
import lambda.core.LParticle;
import lambda.core.LWorld;

/**
 * Main Window of simulation Using buffered canvas with bufferStrategy flipping
 *
 * @author jakub.ziolkowski
 */
public class GameCanvas extends Canvas {

 public int width = 1024, height = 768;
 public static String versionString = "20131203";
 public static GameCanvas canvas;
 public LWorld world;
 private BufferStrategy strategy;
 private Timer timer = null;
 private TimerTask renderTask;
 public LParticle findVertex;

 public GameCanvas() {
  this.setIgnoreRepaint(true);
  timer = new Timer();
  world = new LWorld(this);
 }
 public Graphics2D bkG;

 public void render() {
  try {
   bkG = (Graphics2D) strategy.getDrawGraphics();

   bkG.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
   bkG.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

   bkG.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
   processEvents();

   world.simulate(bkG);

   bkG.dispose();
   strategy.show();
   Toolkit.getDefaultToolkit().sync();
  } catch (Exception e) {
   e.printStackTrace();
  }
 }

 public static void main(String[] args) {
  canvas = new GameCanvas();
  canvas.createAndInit();
 }

 private void createAndInit() {
  final Frame frame = new Frame("Lambda v. " + GameCanvas.versionString);
  frame.setLayout(new BorderLayout());

  frame.add(canvas);
  frame.addWindowListener(
          new WindowAdapter() {
   @Override
   public void windowClosing(WindowEvent e) {
    canvas.stop();
    frame.setVisible(false);
    frame.dispose();
    System.exit(0);
   }
  });
  frame.setSize(width, height);
  frame.setLocationRelativeTo(null);
  frame.setVisible(true);
  canvas.setup();
 }

 protected void stop() {
  renderTask.cancel();
 }
 public LParticle dragPoint = null;

 protected void setup() {

  this.addMouseListener(new MouseListener() {
   @Override
   public void mouseClicked(MouseEvent e) {
   }

   @Override
   public void mousePressed(MouseEvent e) {
    Events.mousePressed = true;
   }

   @Override
   public void mouseReleased(MouseEvent e) {
    findVertex = null;
    Events.mousePressed = false;
   }

   @Override
   public void mouseEntered(MouseEvent e) {
   }

   @Override
   public void mouseExited(MouseEvent e) {
   }
  });

  this.addMouseMotionListener(new MouseMotionListener() {
   @Override
   public void mouseDragged(MouseEvent e) {
    Events.mousePos = e.getPoint();
   }

   @Override
   public void mouseMoved(MouseEvent e) {
    Events.mousePos = e.getPoint();
   }
  });


  this.createBufferStrategy(2);
  strategy = this.getBufferStrategy();
  this.createSimpleWorld();
  start();
 }

 public void start() {
  if (renderTask != null) {
   renderTask.cancel();
  }
  renderTask = new TimerTask() {
   long lasttime = System.currentTimeMillis();

   @Override
   public void run() {
    long time = System.currentTimeMillis();
    lasttime = time;
    render();
   }
  };

  timer.schedule(renderTask, 0, 25);
 }

 private void processEvents() {
  if (Events.mousePressed) {
   // TODO drag
  }
 }

 private void createSimpleWorld() {
  System.out.println("csw");
  for (int i = 1; i < 7; i++) {
   this.world.createBox(i*50, i*20, 30, 50, 1);
  }
  this.world.createBox(100, 100, 100, 100, 50);
  this.world.createBox(100, 300, 500, 200, 100);
  
 }
}
